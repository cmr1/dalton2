import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './login/login.guard';
import { LoginComponent } from './login/login/login.component';
import { MaslGuard } from './masl.guard';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent, canActivate:[LoginGuard]
  },
  {
    path: 'inicio',
    loadChildren: () => import('./panel/vistas.module').then(m => m.VistasModule), canActivate:[MaslGuard]
  },
  {
    path: '**',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
