import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FinancieraComponent } from './catalogos/financiera/financiera.component';
import { InicioComponent } from './inicio/inicio.component';

const routes: Routes = [
  {
    path: '' ,
    component: InicioComponent,
    children:[
      {
        path:'financiera',
        component: FinancieraComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VistasRoutingModule { }
