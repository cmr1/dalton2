import { Component, OnInit } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {of} from 'rxjs';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {


  closeModal: string='';
  showFiller = false;
  constructor(private modalService: NgbModal, private msalService:MsalService) { }

  public usuario = {
    nombre: '',
    usuario: '',
    correo: '',
    perfil: ''
  };
  agregarUsuario(){
    console.log(this.usuario)
  }

  ngOnInit(): void {
  
  }

  getName():any {
    return this.msalService.instance.getActiveAccount()?.name;
  }
  

  triggerModal(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }


}
