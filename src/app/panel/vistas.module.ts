import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './nav/nav.component';
import { InicioComponent } from './inicio/inicio.component';
import { VistasRoutingModule } from './vistas-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from '../material/material.module';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { FormsModule } from '@angular/forms';
import { FinancieraComponent } from './catalogos/financiera/financiera.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    NavComponent,
    InicioComponent,
    UsuariosComponent,
    FinancieraComponent
  ],
  imports: [
    CommonModule,
    VistasRoutingModule,
    NgbModule,
    MaterialModule,
    FormsModule,
    HttpClientModule
  ]
})
export class VistasModule { }
