import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { FinancieraService } from './financiera.service';

@Component({
  selector: 'app-financiera',
  templateUrl: './financiera.component.html',
  styleUrls: ['./financiera.component.scss']
})
export class FinancieraComponent implements OnInit {
  closeModal: string='';
  financieras: any[] =[];

  financiera={
    vchCodigo:'',
    vchDescripcion:''
  }



  constructor(private modalService: NgbModal, private financieraService:FinancieraService) { }

  ngOnInit(): void {

    this.financieraService.mostrarFinancieras().subscribe((res) =>{
      console.log(res);

      this.financieras=res;

    
    })


  }
  

  addFinanciera(){
    this.financieraService.insertarFinanciera(this.financiera).subscribe(res => {
      console.log(res)
    })

    this.financieras.push(this.financiera);
    this.modalService.dismissAll();
  }

  modalEditOpen(contenido:any){
    this.modalService.open(contenido)
  }

  triggerModal(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
