import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FinancieraService {

  readonly url = environment.apiUrl;

  constructor(private http:HttpClient) { }

  public mostrarFinancieras(): Observable<any>{
    return this.http.get<any>(`${this.url}/api/TESCatalogoBanco`);
  }

  public insertarFinanciera(datos: any){
    return this.http.post<any>(`${this.url}/api/TESCatalogoBanco`,datos);
  }

}
