import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../services/usuarios.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  public usuario = {
    nombre: '',
    usuario: '',
    correo: '',
    perfil: ''
  };

  constructor(private usuariosService:UsuariosService) { }

  ngOnInit(): void {
  }
  

  agregarUsuario(){
    this.usuariosService.agregarUsuario(this.usuario).subscribe( resp => {
      console.log(resp)
    })
  }


}
