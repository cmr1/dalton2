import { Component, OnInit } from '@angular/core';
import { MsalService } from '@azure/msal-angular';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(private msalService:MsalService) { }

  ngOnInit(): void {
  }

  logout(){
    this.msalService.logout();
  }

  
  getName():any {
    return this.msalService.instance.getActiveAccount()?.name;
  }
  

}
