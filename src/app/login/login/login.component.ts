import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MsalService } from '@azure/msal-angular';
import { AuthenticationResult } from '@azure/msal-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private msalService:MsalService, private router: Router) { }

  ngOnInit(): void {
    this.msalService.instance.handleRedirectPromise().then(
      res => {
        if(res != null && res.account != null){
          this.msalService.instance.setActiveAccount(res.account)
          this.router.navigate(['inicio']);
        }
      }
    )
  }


  isLoggedIn(): boolean{
    return this.msalService.instance.getActiveAccount()!= null;
  }

  login(){

    this.msalService.loginRedirect();
    
    // this.msalService.loginPopup().subscribe( (response: AuthenticationResult)=>{
    //   console.log(response);
    //   this.msalService.instance.setActiveAccount(response.account)
    //   this.router.navigate(['inicio']);
    // } );
  }


  logout(){
    this.msalService.logout();
    this.router.navigate(['/']);
  }





}
