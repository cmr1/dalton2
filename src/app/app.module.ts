import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MsalModule, MsalService, MSAL_INSTANCE } from '@azure/msal-angular';
import { IPublicClientApplication, PublicClientApplication } from '@azure/msal-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login/login.component';
import { VistasModule } from './panel/vistas.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatSidenavModule} from '@angular/material/sidenav';

export function MSALinstanceFactory(): IPublicClientApplication {
  return new PublicClientApplication({
    auth:{
      clientId: '62f3ad39-bc19-4c58-97aa-ce8c8c6872b4',
      redirectUri:'http://localhost:4200/',
      postLogoutRedirectUri: 'http://localhost:4200'
    }
  })
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule ,
    VistasModule,
    MsalModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: MSAL_INSTANCE,
      useFactory: MSALinstanceFactory
    },
    MsalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
